export const environment = {
  production: true,
  API_SERVER: `https://${window.location.host}/api/v1/`
};
