import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatInputModule, MatButtonModule, MatFormFieldModule, MatToolbarModule,
  MatCardModule, MatTableModule, MatPaginatorModule
} from '@angular/material';
import { ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AuthService } from './services/auth.service';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { HyipListComponent } from './hyip-list/hyip-list.component';
import { TmpComponent } from './tmp/tmp.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { PagerService } from './services/pager.service';
import { RatingComponent } from './rating/rating.component';
import { GoogleAnalyticsEventsService } from './services/gae-service.service';
import { ContactusComponent } from './contactus/contactus.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AboutComponent } from './about/about.component';
import { ConfirmemailComponent } from './confirmemail/confirmemail.component';
import { ThankyouComponent } from './thankyou/thankyou.component';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HyipListComponent,
    TmpComponent,
    PaginatorComponent,
    RatingComponent,
    ContactusComponent,
    TermsComponent,
    PrivacyComponent,
    AboutComponent,
    ConfirmemailComponent,
    ThankyouComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers: [AuthService, PagerService, GoogleAnalyticsEventsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
