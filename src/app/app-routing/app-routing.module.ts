import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { HyipListComponent } from '../hyip-list/hyip-list.component';
import { TmpComponent } from '../tmp/tmp.component';
import { PrivacyComponent } from '../privacy/privacy.component';
import { TermsComponent } from '../terms/terms.component';
import { AboutComponent } from '../about/about.component';
import { ContactusComponent } from '../contactus/contactus.component';
import { ConfirmemailComponent } from '../confirmemail/confirmemail.component';
import { ThankyouComponent } from '../thankyou/thankyou.component';

export const AppRoutes: Routes = [
  { path: 'home', component: TmpComponent},
  { path: 'privacy', component: PrivacyComponent},
  { path: 'terms', component: TermsComponent},
  { path: 'about', component: AboutComponent},
  { path: 'contactus', component: ContactusComponent},
  { path: 'confirmemail', component: ConfirmemailComponent},
  { path: 'thankyou', component: ThankyouComponent},
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
