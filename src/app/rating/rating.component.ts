import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'hyip-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  @Input() stars;
  @ViewChild('rate_stars') ratestars: ElementRef;


  constructor() { }

  ngOnInit() {


    const rating = Math.round(this.stars * 2) / 2;

    const output = [];
    let i;

    // Append all the filled whole stars
    for (i = rating; i >= 1; i--) {
      output.push('<i class="material-icons">star</i>');
    }


    // If there is a half a star, append it
    if (i === .5) {
      output.push('<i class="material-icons">star_half</i>');
    }




    // Fill the empty stars
    for (i = (5 - rating); i >= 1; i--) {
      output.push('<i class="material-icons">star_border</i>');

    }

    this.ratestars.nativeElement.innerHTML = output.join('');


  }

}
