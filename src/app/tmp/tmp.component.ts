import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { PagerService } from '../services/pager.service';
@Component({
  selector: 'hyip-tmp',
  templateUrl: './tmp.component.html',
  styleUrls: ['./tmp.component.scss']
})
export class TmpComponent implements OnInit {


  hyips = [];
  filteredHyips = [];
  // dogs = [{name: 'pippo'}, {name: 'pluto'}, {name: 'ciccio'}];


  constructor(private auth: AuthService, private pagerService: PagerService) { }

  private allItems: any[];

  pager: any = {};

  pagedItems: any[];

  ngOnInit() {
    this.auth.getHyips().subscribe(res => {

      this.allItems = this.filteredHyips = res.body.data;

      // this.hyips = this.filteredHyips = res.body.data;
      // this.filteredHyips = res.body.data;

      this.setPage(1);


    });

    this.auth.valueSearch.subscribe(res => {

      this.allItems = this.filteredHyips.filter(hyip => new RegExp(res, 'gi').test(hyip.name) || new RegExp(res, 'gi').test(hyip.url));
      this.setPage(1);
    });

  }

  setPage(page: number) {

    this.pager = this.pagerService.getPager(this.allItems.length, page);

    console.log(this.pager);

    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }




}
