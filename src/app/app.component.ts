import { Component } from '@angular/core';
import * as moment from 'moment';
import { GoogleAnalyticsEventsService } from './services/gae-service.service';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from './services/auth.service';
declare let ga: Function;



@Component({
  selector: 'hyip-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public auth: AuthService, private router: Router, private gae: GoogleAnalyticsEventsService) {


    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });
  }

  bannerClick(evt) {

    // this.gae.emitEvent('answer', 'answer_from_random', 'answer_from_random', 1);

    if (evt === 'crypto_sniper' ) {
      this.gae.emitEvent('banners', 'banner_click', 'crypto_sniper', 1);

    } else {
      this.gae.emitEvent('banners', 'banner_click', 'fibo', 1);

    }

  }


}
