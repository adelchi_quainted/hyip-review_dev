import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  viewSearch;


  valueSearch: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient, private router: Router) {


  }




  getHyips(): Observable<any> {


    return this.http.get(`${environment.API_SERVER}hyips`, { observe: 'response'});

  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches

    this.valueSearch.next(filterValue);


  }







}
