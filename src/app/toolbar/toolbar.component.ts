import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/index';

@Component({
  selector: 'hyip-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  pathName;
  title;
  routeSub: Subscription;

  constructor(public auth: AuthService, private router: Router, private route: ActivatedRoute) {

    this.routeSub = this.router.events.subscribe(res => {
      if (res instanceof NavigationEnd) {
        this.pathName = res.url;
        this.checkPath();
        window.scrollTo(0, 0);

      }
    });

  }

  checkPath() {
    switch (this.pathName) {
      case '/':
        this.auth.viewSearch = true;
        this.title = '';
        break;
      case '/home':
        this.auth.viewSearch = true;
        this.title = '';
        break;
      case '/privacy':
        this.auth.viewSearch = false;
        this.title = 'Privacy Policy';
        break;
      case '/terms':
        this.auth.viewSearch = false;
        this.title = 'Terms and conditions';
        break;
      case '/about':
        this.auth.viewSearch = false;
        this.title = 'About us';
        break;
      case '/contactus':
        this.auth.viewSearch = false;
        this.title = 'Contact us';
        break;
      case '/confirmemail':
        this.auth.viewSearch = false;
        this.title = 'Confirm your email';
        break;
      case '/thankyou':
        this.auth.viewSearch = false;
        this.title = 'Thanks for contacting us';
        break;
      default:
        this.auth.viewSearch = true;
        this.title = '';
    }
  }

  goHome() {
    this.router.navigate(['/']);
  }


}
