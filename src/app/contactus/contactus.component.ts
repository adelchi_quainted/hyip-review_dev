import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


interface MailChimpResponse {
  result: string;
  msg: string;
}

@Component({
  selector: 'hyip-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent {

  submitted = false;
  mailChimpEndpoint = 'https://quainted.us16.list-manage.com/subscribe/post-json?u=825fbbfe2d66f551558f09615&amp;id=6c0d98b43a&';
  error = '';
  processing = false;

  constructor(private http: HttpClient, private router: Router) { }

  formMC = new FormGroup({

    emailControl : new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    msgControl: new FormControl('', [
      Validators.maxLength(255)])

});

  getErrorMessage() {
    return this.formMC.controls['emailControl'].hasError('required') ? 'You must enter a value' :
      this.formMC.controls['emailControl'].hasError('email') ? 'Not a valid email' :
        '';
  }


  submit() {
    this.error = '';
    if (this.formMC.controls['emailControl'].status === 'VALID') {

      this.processing = true;


      const params = new HttpParams()
        .set('EMAIL', this.formMC.controls['emailControl'].value)
        .set('MESSAGE', this.formMC.controls['msgControl'].value)
        .set('b_825fbbfe2d66f551558f09615_6c0d98b43a', ''); // hidden input name
      const mailChimpUrl = this.mailChimpEndpoint + params.toString();

      // 'c' refers to the jsonp callback param key. This is specific to Mailchimp
      this.http.jsonp<MailChimpResponse>(mailChimpUrl, 'c').subscribe(response => {
        console.log(response);
        if (response.result && response.result !== 'error') {

          this.submitted = true;
          console.log('subtmitted');
          this.router.navigate(['confirmemail']);
        } else {
          this.processing = false;
          this.error = response.msg;
        }
      }, error => {
        this.processing = false;
        console.error('ops', error);
        this.error = 'Sorry, an error occurred.';
      });
    }
  }

}
